///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    hello.c
/// @version 1.0 - Initial version
///
/// "Hello World!" programs are the most basic program that can be written and
/// are used to demonstrate the correct operation of the edit-compile-run
/// toolchain.
///
/// @author  Zack Lown <zlown@hawaii.edu>
<<<<<<< HEAD
/// @date    13/Jan/2022
=======
// @date     13/1/2022
>>>>>>> 11472e711e6b13b047535c77d92d7c36da1d9e38
///
/// @see     https://en.wikipedia.org/wiki/%22Hello,_World!%22_program
/// @see     https://www.thesoftwareguild.com/blog/the-history-of-hello-world/
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
	printf( "Hello world!\n" );
}

